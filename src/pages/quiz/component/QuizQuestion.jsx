import { FaAngleRight, FaChevronLeft, FaChevronRight } from "react-icons/fa";
import _ from 'lodash';

export default function QuizQuestion({ 
  question, 
  indexQuestion,
  handleNextQuestion,
  handlePrevQuestion
}) {

  if(_.isEmpty(question)) return <></>

  return (
    <div className="w-4/6 z-10 px-10 py- shadow-lg rounded-xl text-center">
      <h1 className="z-10 px-32 text-center py-5 font-medium text-2xl font-semibold mb-4 bg-white text-primary rounded-full">
        Câu{indexQuestion+1}: {question?.title}
      </h1>

      <div className="px-32 relative ml-3 cursor-pointer flex flex-col gap-4 text-left">
        <p className="px-2 py-4 rounded-full border pl-4 bg-white rounded-full border hover:bg-gradient-primary hover:text-white hover:font-semibold">
          {question?.option_a}
        </p> 
        <p className="px-2 py-4 rounded-full border pl-4 bg-white rounded-full border hover:bg-gradient-primary hover:text-white hover:font-semibold">
          {question?.option_b}
        </p> 
        <p className="px-2 py-4 rounded-full border pl-4 bg-white rounded-full border hover:bg-gradient-primary hover:text-white hover:font-semibold">
          {question?.option_c}
        </p> 
        <p className="px-2 py-4 rounded-full border pl-4 bg-white rounded-full border hover:bg-gradient-primary hover:text-white hover:font-semibold">
          {question?.option_d}
        </p>
        <button onClick={() => handlePrevQuestion()} className="absolute center top-1/2 left-0 w-8 h-8 flex items-center rounded-full shadow-xl bg-white hover:bg-gradient-primary hover:text-white" ><FaChevronLeft className="flex-1"/></button>
        <button onClick={() => handleNextQuestion()}  className="absolute top-1/2 right-0 w-8 h-8 flex items-center rounded-full shadow-xl bg-white text-center hover:bg-gradient-primary hover:text-white" ><FaChevronRight className="flex-1"/></button>
      </div>

        
    </div>
  );
}

import useSWR from "swr";
import api from "../../apis/httpClient";
import { useParams } from "react-router-dom";
import QuizQuestion from "./component/QuizQuestion";
import { useEffect, useState } from "react";
import QuizHelper from "./component/QuizHelper";

export default function Quiz() {
  const { quizId } = useParams();
  const [quizList, setQuizList] = useState([]);
  const [indexQuestion, setIndexQuestion] = useState(0);

  useEffect(() => {
    const fetchQuiz = async () => {
      try {
        const res = await api.get(`/quiz/generateQuestions/${quizId}`);
        if (!res.data.data) {
          console.log("No data found");
          return;
        }
        setQuizList(res?.data?.data?.questions);

        // store quizResultId in sessionStorage
        sessionStorage.setItem(
          "quizResultId",
          JSON.stringify(res?.data?.data?.quizResultId)
        );
      } catch (error) {
        console.log(error);
      }
    };

    fetchQuiz();
  }, [quizId]);

  const handleNextQuestion = () => {
    setIndexQuestion(indexQuestion + 1);
  };
  const handlePrevQuestion = () => {
    setIndexQuestion(indexQuestion - 1);
  };

  return (
    <div className="flex justify-center pt-20 gap-8 h-screen bg-second-background bg-cover">
      <QuizQuestion
        handleNextQuestion={handleNextQuestion}
        handlePrevQuestion={handlePrevQuestion}
        indexQuestion={indexQuestion}
        question={quizList.length > 0 ? quizList[indexQuestion] : {}}
      />
      <QuizHelper />
    </div>
  );
}

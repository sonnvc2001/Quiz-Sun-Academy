import { useState } from "react";
import QuizItem from "./QuizItem";
import api from "../../../apis/httpClient";
import useSWR from "swr";

export default function QuizList() {
  const [quizList, setQuizList] = useState([]);
  const { data, error, isLoading } = useSWR('/quiz/getQuizByUser', api);

  return (
    <div className="bg-primary-background bg-cover h-screen">
    <h2 className="text-center text-3xl font-semibold text-white py-9">Danh sách bài thi</h2>
     <div className="flex items-center justify-center">
      <div className="grid grid-cols-3 gap-9 justify-center">
        {
          data && data?.data?.data?.map((quiz) => (
            <QuizItem key={quiz.id} data={quiz} />
          ))
        }
      </div>
    </div>
    </div>
   
  );
}

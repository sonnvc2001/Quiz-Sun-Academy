import Header from "../../layouts/Header";
import QuizList from "./component/QuizList";

export default function Home() {

  return (
    <div className="">
     <QuizList />
    </div>
  );
}

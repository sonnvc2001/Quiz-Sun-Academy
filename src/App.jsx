import { Route, Routes } from "react-router-dom";
import { Toaster } from "react-hot-toast";
import Register from "./components/user/Register";
import { Provider } from "react-redux";
import store from "./store";
import Login from "./pages/auth/Login/Login.jsx";
import PrivateRouter from "./routers/PrivateRoute.jsx";
import Home from "./pages/home/Home.jsx";
import Quiz from "./pages/quiz/Quiz.jsx";
import Main from "./layouts/Main.jsx";

function App() {
  return (
    <>
      <Toaster position="top-center" toastOptions={{ duration: 3000 }} />
      <Provider store={store}>
        <Routes>
        <Route element={<Main />}>
          <Route >
            <Route
             path="/"
              element={
                <PrivateRouter>
                  <Home />
                </PrivateRouter>
              }
            />
            <Route
              path="/quiz/:quizId"
              element={
                <PrivateRouter>
                  <Quiz />
                </PrivateRouter>
              }
            />
          </Route>
        </Route>
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
        </Routes>
      </Provider>
    </>
  );
}

export default App;
